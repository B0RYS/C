// Standard library.
#include <stdio.h>

// Other libs.
#include <string.h>


// Define booleans as C doesnt natively support them.
#define bool char
#define false 0
#define true 1

void main () {
    // Define variables.
    bool a = false; // Boolean.
    int b = 1; // Integer (-2147483648 to 2147483647).
    float decimal = 54.33; // Float (1.175494e-38 to 3.402823e+38).
    char * word = "Hello"; // String.
    double doub = 6.9; // Double (2.225074e-308 to 1.797693e+308)

    // Change the value of "b" to true so that the if statement is executed.
    a = true;
    if (a == true) {
        // Print if a is true.
        printf("The value of the variable \"b\" is %d\n", b);
    };

    // Basic arithemtic.
    int total = 0, a1 = 2,b1 = 4, c = 6;

    // Total a1, b1 and c.
    total = a1 + b1 + c;

    // Print total.
    printf("The sum of 2, 4 and 6 equates to: %d.\n", total);

    // Print word.
    printf("%s: %s", word);
};