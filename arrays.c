#include <stdio.h>

void main () {
    // Define an array: <type> <nameofvar>[<size>].
    long int array[5] = {1, 2, 3, 4, 5};


    // Sum of the array.
    int total = 0;

    // Loop and add.
    for (int i = 0; i < 5; i++) {
        total = total + array[i];
    };

    printf("The sum of the array equates to %d.\n", total); 

    // Multidimensional array.
    int array1[1][2] = {
        {3, 4}
    };

    printf("Second element in the second dimension of the array: %d\n", array1[0][1]);
};